# RunnerApp

## Dependencies

- Vite
- React
- Ionic
- TypeScript
- React-Redux
- React-Redux-Toolkit
- React-Router-Dom

## Environment configuration

Before running the application, make sure to add the following environment variables to your `.env` file:

```env
VITE_STRAVA_API_BASE_URL=https://www.strava.com/api/v3
VITE_STRAVA_API_BEARER_TOKEN=
```
### Install dependencies


```bash
yarn install
```

### Run project

```bash
yarn dev
```
