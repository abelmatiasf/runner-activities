
import React from 'react';
import { IonBackButton, IonButtons, IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import ActivityList from './ActivityList';
import { RootState } from '../store';
import { useSelector } from 'react-redux';

const FilteredMonthlyActivities: React.FC = () => {
   const filteredActivities = useSelector((state: RootState) => state.activities.filteredList);

   return (
      <IonPage>
         <IonHeader>
            <IonToolbar color="tertiary">
               <IonButtons slot="start">
                  <IonBackButton defaultHref={filteredActivities.from} />
               </IonButtons>
               <IonTitle>{`Activities in ${filteredActivities.label}`}</IonTitle>
            </IonToolbar>
         </IonHeader>
         <IonContent>
            <ActivityList activities={filteredActivities.results} />
         </IonContent>
      </IonPage>
   );
};

export default FilteredMonthlyActivities;
