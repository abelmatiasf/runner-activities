import React from 'react';
import { IonItem, IonList } from '@ionic/react';
import ActivityItem from './ActivityItem';
import { Activity } from '../features/activities/interfaces/Activity';

interface ActivityListProps {
  activities: Activity[];
}

const ActivityList: React.FC<ActivityListProps> = ({ activities = [] }) => {
  console.log({activities});
  return (
    <>
      <IonList>
        {activities.length > 0 ? (activities.map(activity => (
          <>
            <ActivityItem key={activity.id} activity={activity} />
          </>
        ))) : <IonItem>Loading items...</IonItem>}
      </IonList>
    </>
  );
};

export default ActivityList;
