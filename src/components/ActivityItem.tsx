import React from 'react';
import { IonItem, IonLabel, IonIcon, IonThumbnail, IonChip } from '@ionic/react';
import { timerSharp } from 'ionicons/icons';

interface ActivityItemProps {
  activity: {
    id: number;
    name: string;
    distance: number;
    moving_time: number;
    total_elevation_gain: number;
    type: string;
    sport_type: string;
    start_date_local: string;
    timezone: string;
    location_country: string;
    max_speed: number;
  };
}

const ActivityItem: React.FC<ActivityItemProps> = ({ activity }) => (
  <IonItem key={activity.id}>
    <IonThumbnail>
      <IonIcon color='tertiary' icon={timerSharp} size="large"></IonIcon>
    </IonThumbnail>
    <IonLabel>
      <p><IonChip color="tertiary">Name</IonChip> {activity.name}</p>
      <p><IonChip color="tertiary">Date</IonChip> {new Date(activity.start_date_local).toDateString()}</p>
      <p><IonChip color="tertiary">Distance</IonChip> {activity.distance / 1000} km</p>
      <p><IonChip color="tertiary">Time</IonChip> {activity.moving_time / 60} minutes</p>
      <p><IonChip color="tertiary">Elevation Gain</IonChip> {activity.total_elevation_gain} meters</p>
    </IonLabel>
  </IonItem>
);

export default ActivityItem;
