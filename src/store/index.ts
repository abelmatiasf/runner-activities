import { configureStore } from '@reduxjs/toolkit';
import activitiesReducer from '../features/activities/activitiesSlice';

const store = configureStore({
    reducer: {
        activities: activitiesReducer,
        // Agrega otros slices aquí si es necesario
    },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;