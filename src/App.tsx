/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";
import "@ionic/react/css/core.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";
import "./theme/default.css";

import React from 'react';
import { IonApp, IonIcon, IonLabel, IonRouterOutlet, IonTabBar, IonTabButton, IonTabs, setupIonicReact } from '@ionic/react';
import { calendar, list } from 'ionicons/icons';
import { IonReactRouter } from '@ionic/react-router';
import { Route, Redirect } from 'react-router-dom';
import MyActivity from './pages/MyActivity';
import MonthlyActivities from "./pages/MonthlyActivities";
import FilteredMonthlyActivities from "./components/FilteredMonthlyActivities";

setupIonicReact();

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonRouterOutlet>
        <IonTabs>
          <IonRouterOutlet>
            <Redirect exact from="/" to="/activities" />

            {/* Rutas de páginas individuales */}
            <Route exact path="/activities" component={MyActivity} />
            <Route path="/monthly" exact component={MonthlyActivities} />
            <Route path="/monthly/:month" component={FilteredMonthlyActivities} />
            
          </IonRouterOutlet>

          {/* Pestañas de navegación */}
          <IonTabBar slot="bottom">
            <IonTabButton tab="activities" href="/activities">
              <IonIcon icon={list} size="large"></IonIcon>
              <IonLabel>Activities</IonLabel>
            </IonTabButton>

            <IonTabButton tab="monthly" href="/monthly" >
              <IonIcon icon={calendar} size="large"></IonIcon>
              <IonLabel>Monthly</IonLabel>
            </IonTabButton>
          </IonTabBar>
        </IonTabs>
      </IonRouterOutlet>
    </IonReactRouter>
  </IonApp>
);

export default App;
