import React, { useEffect } from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import ActivityList from '../components/ActivityList';
import { useDispatch, useSelector } from 'react-redux';
import { fetchRecentActivities } from '../features/activities/activitiesSlice';
import { UnknownAction } from 'redux';
import { RootState } from '../store';

const MyActivity: React.FC = () => {
  const dispatch = useDispatch();
  const listActivities = useSelector((state: RootState) => state.activities.list);

  useEffect(() => {
    dispatch(fetchRecentActivities() as unknown as UnknownAction);
  }, [dispatch]);
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="tertiary">
          <IonTitle>Activities</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <ActivityList activities={listActivities}/>
      </IonContent>
    </IonPage>
  )
};

export default MyActivity;