import React, { useMemo } from 'react';
import { IonHeader, IonPage, IonTitle, IonToolbar, IonList, IonItem, IonLabel, IonIcon } from '@ionic/react';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../store';
import { arrowForward } from 'ionicons/icons';
import { useHistory } from 'react-router-dom';
import { setFilteredActivites } from '../features/activities/activitiesSlice';

const MonthlyActivities: React.FC = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const activities = useSelector((state: RootState) => state.activities.list);

  // Memorizar el cálculo de groupedActivities
  const groupedActivities = useMemo(() => {
    return activities.reduce((added, activity) => {
      const startDate = new Date(activity.start_date_local);
      const monthKey = `${startDate.toLocaleString('en-US', { month: 'long' })} ${startDate.getFullYear()}`;

      if (!added[monthKey]) {
        added[monthKey] = [];
      }

      added[monthKey].push(activity);
      return added;
    }, {} as { [key: string]: any });
  }, [activities]);

  // Obtener los últimos 3 meses en formato deseado
  const recentMonths = useMemo(() => {
    return Object.keys(groupedActivities)
      .slice(0, 3)
      .map(month => ({ label: month.split(' ')[0] }));
  }, [groupedActivities]);


  // Función para filtrar y despachar
  const toFilteredAndDispatch = useMemo(() => {
    return (selectedMonth: string) => {
      // Filtrar las actividades por el mes seleccionado
      const filteredActivities = activities.filter(activity => {
        const activityMonth = new Date(activity.start_date_local).toLocaleString('en-US', { month: 'long' });
        return activityMonth === selectedMonth;
      });

      dispatch(setFilteredActivites({ 
        results: filteredActivities, 
        label: selectedMonth.toLowerCase(),
        from: "monthly"
      }));
      
      history.push(`/monthly/${selectedMonth.toLowerCase()}`);
    };
  }, [history, activities]);


  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="tertiary">
          <IonTitle>Monthly Stats</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonList>
        {recentMonths.map((monthObj, index) => (
          <IonItem key={index} button onClick={() => toFilteredAndDispatch(monthObj.label)}>
            <IonLabel>{monthObj.label}</IonLabel>
            <IonIcon icon={arrowForward} slot="end" color='tertiary' />
          </IonItem>
        ))}
      </IonList>
    </IonPage>
  );
};

export default MonthlyActivities;
