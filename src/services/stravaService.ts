import axios from 'axios';
import { ActivitesByFixture } from '../fixture/activities';
/**
 * 
 * @param accessToken 
 * @returns Athlete activities
 */
const API_BASE_URL = import.meta.env.VITE_STRAVA_API_BASE_URL ? import.meta.env.VITE_STRAVA_API_BASE_URL : null;
export const getRecentActivities = async (accessToken: string) => {
  try {
    if (API_BASE_URL) {
      const response = await axios.get(`${API_BASE_URL}/athlete/activities`, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });
      return response.data;
    }else{
      return ActivitesByFixture;
    }
  } catch (error) {
    console.error('Error fetching activities from Strava:', error);
    console.warn("charged activities from fixture");
    return ActivitesByFixture;
  }
};
