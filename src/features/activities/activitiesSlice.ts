import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { getRecentActivities } from '../../services/stravaService';
import { ActivitiesState } from './interfaces/ActivitiesState';
import { FilteredList } from './interfaces/FilteredList';

const initialState: ActivitiesState = {
  list: [],
  filteredList: {
    from: "",
    label: "",
    results: []
  },
  status: 'idle',
  error: null,
};

// Crear una acción asíncrona para obtener actividades recientes
export const fetchRecentActivities = createAsyncThunk('activities/fetchRecentActivities', async () => {
  const accessToken = import.meta.env.VITE_STRAVA_API_BEARER_TOKEN;
  const response = await getRecentActivities(accessToken);

  return response;
});

const activitiesSlice = createSlice({
  name: 'activities',
  initialState,
  reducers: {
    setFilteredActivites: (state, action: PayloadAction<FilteredList>) => {
      state.filteredList.results = action.payload.results;
      state.filteredList.from = action.payload.from;
      state.filteredList.label = action.payload.label;
    }
  },
  extraReducers: builder => {
    builder
      .addCase(fetchRecentActivities.pending, state => {
        state.status = 'loading';
      })
      .addCase(fetchRecentActivities.fulfilled, (state, action) => {
        state.status = 'succeeded';
        state.list = action.payload;
      })
      .addCase(fetchRecentActivities.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error.message ?? 'Error fetching activities';
      });
  },
});

export const { setFilteredActivites } = activitiesSlice.actions;
export default activitiesSlice.reducer;
