
export interface Activity {
  id: number;
  name: string;
  distance: number;
  moving_time: number;
  total_elevation_gain: number;
  type: string;
  sport_type: string;
  start_date_local: string;
  timezone: string;
  location_country: string;
  max_speed: number;
}
