
export interface Map {
  id: string;
  summary_polyline: string;
  resource_state: number;
}
