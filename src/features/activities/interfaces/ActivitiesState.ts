import { Activity } from './Activity';
import { FilteredList } from './FilteredList';

export interface ActivitiesState {
  list: Activity[];
  filteredList: FilteredList;
  status: 'idle' | 'loading' | 'succeeded' | 'failed';
  error: string | null;
}
