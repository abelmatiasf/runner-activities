import { Activity } from './Activity';

export interface FilteredList {
  from?: string;
  label?: string;
  results: Activity[];
}
